from random import randint

class Field:
    x = 32 - 1
    y = 24 - 1

    xBonus = 0;
    yBonus = 0;

    bonusBackground = "#FF0000"

    def __init__(self):
        self.generateBonusCell()

    def generateBonusCell(self):
        self.xBonus = randint(0, self.x)
        self.yBonus = randint(0, self.y)

