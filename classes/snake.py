from classes.cell import *
from classes.field import *

class Snake:
    defaultLength = 5
    point = 0;
    field = Field()
    isActive = True
    way = "right"
    points = [Cell(i, 0) for i in range(0, 5)]

    def __init__(self):
        self.points.reverse()

    def validateStep(self, x, y):
        #validate bounds
        if x > self.field.x or x < 0 or y > self.field.y or y < 0:
            self.isActive = False

        #snake to snake
        for p in self.points:
            if p.x == x and p.y == y:
                self.isActive = False

        #take bonus
        if self.field.xBonus == x and self.field.yBonus == y:
            self.points.insert(0, Cell(x, y))
            self.point += 1
            self.field.generateBonusCell()


    def doOneStep(self, x, y):
        self.validateStep(x,y)
        if self.isActive:
            self.points = self.points[:len(self.points) - 1]
            self.points.insert(0, Cell(x, y))

    #Snake movement
    ##Positioning on the snake
    def doForwardStep(self):
        self.doOneStep(self.points[0].x + 1, self.points[0].y)

    def doBackStep(self):
        self.doOneStep(self.points[0].x - 1, self.points[0].y)

    def doLeftStep(self):
        self.doOneStep(self.points[0].x, self.points[0].y - 1)

    def doRightStep(self):
        self.doOneStep(self.points[0].x, self.points[0].y + 1)

    ##Positioning on the user
    def go(self):
        if self.way == "right":
            self.doForwardStep()
        elif self.way == "left":
            self.doBackStep()
        elif self.way == "up":
            self.doLeftStep()
        else:
            self.doRightStep()

    def Up(self):
        if self.way != "down":
            self.way = "up"

    def Down(self):
        if self.way != "up":
            self.way = "down"

    def Left(self):
        if self.way != "right":
            self.way = "left"

    def Right(self):
        if self.way != "left":
            self.way = "right"