import pygame
from pygame import *
from classes.snake import *
import time
from pygame.locals import *

snake = Snake()

#Объявляем переменные
WIN_WIDTH = 512 #Ширина создаваемого окна
WIN_HEIGHT = 384 # Высота
DISPLAY = (WIN_WIDTH, WIN_HEIGHT) # Группируем ширину и высоту в одну переменную
BACKGROUND_COLOR = "#FFFFFF"

pygame.init()
screen = pygame.display.set_mode(DISPLAY) #Создаём окошко
pygame.display.set_caption("Test App") # Пишем в шапку
bg = Surface((WIN_WIDTH, WIN_HEIGHT)) # Создание видимой поверхности # будем использовать как фон
bg.fill(Color(BACKGROUND_COLOR))    # Заливаем поверхность сплошным цветом


millis = int(round(time.time() * 1000))
running = True
while running: # Основной цикл программы
    for e in pygame.event.get(): # Обрабатываем события
        if e.type == pygame.QUIT:
            running = False

        #keys
        if e.type == KEYDOWN and e.key == K_UP: snake.Up()
        if e.type == KEYDOWN and e.key == K_DOWN: snake.Down()
        if e.type == KEYDOWN and e.key == K_RIGHT: snake.Right()
        if e.type == KEYDOWN and e.key == K_LEFT: snake.Left()

    screen.blit(bg, (0, 0))      # Каждую итерацию необходимо всё перерисовывать

    #program methods
    ##draw snake
    for p in snake.points:
        pf = Surface((15, 15))
        pf.fill(Color(p.background_color))
        screen.blit(pf, (p.x * 16, p.y * 16))

    if int(round(time.time() * 1000)) - millis > 500 - snake.point*10:
        millis = int(round(time.time() * 1000))
        if snake.isActive:
            snake.go()

    ##draw bonus
    pf = Surface((15, 15))
    pf.fill(Color(snake.field.bonusBackground))
    screen.blit(pf, (snake.field.xBonus * 16, snake.field.yBonus * 16))


    # Display some text
    # font = pygame.font.Font(None, 24)
    # text = font.render(str(snake.point), 1, (100, 100, 100))
    # textpos = text.get_rect()
    # textpos.centerx = bg.get_rect().centerx
    # bg.blit(text, textpos)
    myfont = pygame.font.SysFont("monospace", 24)
    label = myfont.render(str(snake.point), 1, (100, 100, 100))
    textpos = label.get_rect()
    textpos.centerx = bg.get_rect().centerx
    screen.blit(label, textpos)

    pygame.display.update()     # обновление и вывод всех изменений на экран


pygame.quit()